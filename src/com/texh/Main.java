package com.texh;

import com.texh.colorspaces.CMY;
import com.texh.colorspaces.HSL;
import com.texh.colorspaces.RGB;

public class Main {

  public static void main(String[] args) {

    //Hieronder wordt om functionaliteit te bewijzen iedere functie aangeroepen zoals gedefinieerd in de opdracht.
    System.out.println("RGBtoCMY Functie RGB = 0.4, 0.6, 0.8");
    CMY cmy = RGBtoCMY(0.4f, 0.6f, 0.8f);
    cmy.Print();

    System.out.println("--------------------------------");

    System.out.println("CMYtoRGB Functie CMY = 0.6, 0.4, 0.2");
    RGB rgb = CMYtoRGB(0.6f, 0.4f, 0.2f);
    rgb.Print();

    System.out.println("--------------------------------");

    System.out.println("RGBtoHSL Functie RGB = 0.4, 0.6, 0.8");
    HSL hsl = RGBtoHSL(0.4f, 0.6f, 0.8f);
    hsl.Print();

    System.out.println("--------------------------------");

    System.out.println("HSLtoRGB Functie HSL = 210, 0.5, 0.6");
    RGB rgbX = HSLtoRGB(210, 0.5f, 0.6f);
    rgbX.Print();

    System.out.println("--------------------------------");

    System.out.println("Transparency Functie");
    System.out.println("RGB Front = 0.4, 0.4, 0.8, 200");
    System.out.println("RGB Back = 0.8, 0.6, 0.5");
    RGB rgbSum = Transparency(0.4f, 0.4f, 0.8f, 200, 0.8f, 0.6f, 0.5f);
    rgbSum.Print();
  }

  // Functies zoals gedefinieerd in de opdracht.

  public static CMY RGBtoCMY(float r, float g, float b) {
    RGB rgb = new RGB(r, g, b);
    return rgb.toCMY();
  }

  public static RGB CMYtoRGB(float c, float m, float y) {
    CMY cmy = new CMY(c, m, y);
    return cmy.toRGB();
  }

  public static HSL RGBtoHSL(float r, float g, float b) {
    RGB rgb = new RGB(r, g, b);
    return rgb.toHSL();
  }

  public static RGB HSLtoRGB(float h, float s, float l) {
    HSL hsl = new HSL(h, s, l);
    return hsl.toRGB();
  }

  public static RGB Transparency(float rf, float gf, float bf, int a, float rb, float gb, float bb) {
    RGB rgbFront = new RGB(rf, gf, bf, a);
    RGB rgbBack = new RGB(rb, gb, bb);
    Blender blender = new Blender();

    return blender.RGBblender(rgbFront, rgbBack);
  }

}