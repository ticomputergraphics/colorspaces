package com.texh;

import com.texh.colorspaces.RGB;

public class Blender {

    public RGB RGBblender(RGB Front, RGB Back) {
        float a = Front.getOpacity();

        RGB rgb = new RGB();

        rgb.setRed(a*Front.getRed()+((255-a)*Back.getRed()));
        rgb.setGreen(a*Front.getGreen()+((255-a)*Back.getGreen()));
        rgb.setBlue(a*Front.getBlue()+((255-a)*Back.getBlue()));

        return rgb;

    }


}
