package com.texh.colorspaces;

public class HSL {
  float hue;
  float saturation;
  float lightness;

  // Constructor voor een HSL kleur object
  public HSL(float hue, float saturation, float lightness) {
    this.hue = hue;
    this.saturation = saturation;
    this.lightness = lightness;
  }

  // Functie die HSL kleur vertaalt naar een RGB kleur object
  public RGB toRGB(){
    float h = (this.hue/360);
    float s = this.saturation;
    float l = this.lightness;

    float r, g, b;

    if (s == 0f) {
      r = g = b = l; // achromatic
    } else {
      float q = l < 0.5f ? l * (1 + s) : l + s - l * s;
      float p = 2 * l - q;
      r = HUEtoRGB(p, q, h + 1f/3f);
      g = HUEtoRGB(p, q, h);
      b = HUEtoRGB(p, q, h - 1f/3f);
    }

    return new RGB(r, g, b);
  }

  public float HUEtoRGB(float p, float q, float t) {
    if (t < 0f)
      t += 1f;
    if (t > 1f)
      t -= 1f;
    if (t < 1f/6f)
      return p + (q - p) * 6f * t;
    if (t < 1f/2f)
      return q;
    if (t < 2f/3f)
      return p + (q - p) * (2f/3f - t) * 6f;
    return p;
  }


  public void Print(){
    System.out.println("Hue = " + hue
            + " Saturation = " + saturation
            + " Lightness = " + lightness);
  }



  // -----  Getters en Setters ----- //

  public float getHue() {
    return hue;
  }

  public void setHue(float hue) {
    this.hue = hue;
  }

  public float getSaturation() {
    return saturation;
  }

  public void setSaturation(float saturation) {
    this.saturation = saturation;
  }

  public float getLightness() {
    return lightness;
  }

  public void setLightness(float lightness) {
    this.lightness = lightness;
  }
}
