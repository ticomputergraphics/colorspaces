package com.texh.colorspaces;

public class CMY {
  float cyan;
  float magenta;
  float yellow;


  // Constructor voor het aan maken van een CMY kleur object
  public CMY(float cyan, float magenta, float yellow) {
    this.cyan = cyan;
    this.magenta = magenta;
    this.yellow = yellow;
  }

  // Functie die een CMY kleur object vertaalt naar een RGB kleur object
  public RGB toRGB(){
    RGB rgb = new RGB(1f-this.cyan, 1f-this.magenta, 1f-this.yellow);
    return rgb;
  }

  public void Print(){
    System.out.println(
            "Cyan = " + cyan
            + " Magenta = " + magenta
            + " Yellow = " + yellow);
  }

  // -----  Getters en Setters ----- //

  public float getCyan() {
    return cyan;
  }

  public void setCyan(float cyan) {
    this.cyan = cyan;
  }

  public float getMagenta() {
    return magenta;
  }

  public void setMagenta(float magenta) {
    this.magenta = magenta;
  }

  public float getYellow() {
    return yellow;
  }

  public void setYellow(float yellow) {
    this.yellow = yellow;
  }
}
