package com.texh.colorspaces;

import static java.sql.Types.NULL;

public class RGB {
  float red;
  float green;
  float blue;
  int opacity;

  public RGB() {

  }

  // Constructor voor RGB kleur zonder transparantie.
  public RGB(float red, float green, float blue) {
    this.red = red;
    this.blue = blue;
    this.green = green;
  }

  // Constructor voor RGB kleur met transparantie
  public RGB(float red, float green, float blue, int opacity) {
    this.red = red;
    this.blue = blue;
    this.green = green;
    this.opacity = opacity;
  }

  // Functie die RGB kleur object vertaalt naar CMY kleur object.

  public CMY toCMY() {
    CMY cmy = new CMY(1f - this.red, 1f - this.green, 1f - this.blue);
    return cmy;
  }


  // Functie die RGB kleur object vertaalt naar HSL kleur object
  public HSL toHSL() {
    float r = this.red;
    float g = this.green;
    float b = this.blue;

    // Minimum and Maximum RGB waarden om HSL waarden te berekenen.

    float min = Math.min(r, Math.min(g, b));
    float max = Math.max(r, Math.max(g, b));


    // Hue berekening

    float h = 0;

    if (max == min)
      h = NULL;
    else if (r == max && b == min)
      h = (60 * ((g - min) / (max - min)));
    else if (g == max && b == min)
      h = ((60 * ((max - r) / (max - min))) + 60);
    else if (g == max && r == min)
      h = ((60 * ((b - min) / (max - min))) + 120);
    else if (b == max && r == min)
      h = ((60 * ((max - g) / (max - min))) + 180);
    else if (b == max && g == min)
      h = ((60 * ((r - min) / (max - min))) + 240);
    else if (r == max && g == min)
      h = ((60 * ((max - b) / (max - min))) + 300);


    // Lightness berekening

    float l = (max + min) / 2;

    // Saturation berekening

    float s = 0;

    if (max == min)
      s = 0;
    else if (l <= .5f)
      s = (max - min) / (max + min);
    else
      s = (max - min) / (2 - max - min);

    return new HSL(h, s, l);
  }

  public void Print() {
    System.out.println(
            "Red = " + red
            + " Green = " + green
            + " Blue = " + blue);
  }

  // -----  Getters en Setters ----- //

  public float getRed() {
    return red;
  }

  public void setRed(float red) {
    this.red = red;
  }

  public float getGreen() {
    return green;
  }

  public void setGreen(float green) {
    this.green = green;
  }

  public float getBlue() {
    return blue;
  }

  public void setBlue(float blue) {
    this.blue = blue;
  }

  public int getOpacity() {
    return opacity;
  }

  public void setOpacity(int opacity) {
    this.opacity = opacity;
  }
}
